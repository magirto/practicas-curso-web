$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000})
  });
  $('#modalInquietudes').on('show.bs.modal',function(e){
       console.log(' Se está mostrando el modal de inquietudes')
      $('#inquietudes-btn').removeClass("btn-outline-success");
      $('#inquietudes-btn').addClass("btn-primary");
      $('#inquietudes-btn').prop('disabled',true);
  });

  $('#modalInquietudes').on('shown.bs.modal',function(e){
       console.log(' Se mostró el modal de inquietudes')

  });

  $('#modalInquietudes').on('hide.bs.modal',function(e){
       console.log(' Se está ocultando el modal de inquietudes')
        $('#inquietudes-btn').removeClass("btn-primary");
        $('#inquietudes-btn').addClass("btn-outline-success");
        $('#inquietudes-btn').prop('disabled',false);
  });

  $('#modalInquietudes').on('hidden.bs.modal',function(e){
       console.log(' Se ocultó el modal de inquietudes')

  });